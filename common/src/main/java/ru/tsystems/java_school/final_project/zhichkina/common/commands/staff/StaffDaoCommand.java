package ru.tsystems.java_school.final_project.zhichkina.common.commands.staff;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.RequestCommand;

/**
 * @author Olga Zhichkina
 */
public interface StaffDaoCommand extends RequestCommand {
}
