package ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Passenger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;

import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class ViewTrainsInfoResponse implements Command {
    List<TimeTableLine> lines;
    List<Passenger> passengers;
    List<String> stationNames;

    public List<TimeTableLine> getLines() {
        return lines;
    }

    public void setLines(List<TimeTableLine> lines) {
        this.lines = lines;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public List<String> getStationNames() {
        return stationNames;
    }

    public void setStationNames(List<String> stationNames) {
        this.stationNames = stationNames;
    }
}
