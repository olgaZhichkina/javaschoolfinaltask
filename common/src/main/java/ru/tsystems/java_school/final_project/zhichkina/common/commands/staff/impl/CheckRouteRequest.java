package ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.StaffDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class CheckRouteRequest implements StaffDaoCommand {
    String trainName;

    @Override
    public String operationName() {
        return "checkRoute";
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }
}
