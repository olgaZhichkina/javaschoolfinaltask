package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;

/**
 * @author Olga Zhichkina
 */
public class GetRightsResponse implements Command {
    Integer result;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
