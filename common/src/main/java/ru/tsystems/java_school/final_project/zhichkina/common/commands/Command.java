package ru.tsystems.java_school.final_project.zhichkina.common.commands;

import java.io.Serializable;

/**
 * @author Olga Zhichkina
 */
public interface Command extends Serializable {
}
