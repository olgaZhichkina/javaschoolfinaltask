package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.PassengerDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class GetRightsRequest implements PassengerDaoCommand {
    long passengerId;

    @Override
    public String operationName() {
        return "getRights";
    }

    public long getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(long passengerId) {
        this.passengerId = passengerId;
    }
}
