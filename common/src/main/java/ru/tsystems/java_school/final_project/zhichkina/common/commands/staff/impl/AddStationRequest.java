package ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.StaffDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class AddStationRequest implements StaffDaoCommand {
    private String name;

    @Override
    public String operationName() {
        return "addStation";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
