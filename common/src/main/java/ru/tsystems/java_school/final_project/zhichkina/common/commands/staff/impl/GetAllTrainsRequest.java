package ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.StaffDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class GetAllTrainsRequest implements StaffDaoCommand {

    @Override
    public String operationName() {
        return "getAllTrains";
    }
}
