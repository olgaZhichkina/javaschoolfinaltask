package ru.tsystems.java_school.final_project.zhichkina.common.commands;

/**
 * @author Olga Zhichkina
 */
public interface RequestCommand extends Command {
    String operationName();
}
