package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Ticket;

import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class GetMyTicketsResponse implements Command {
    List<Ticket> tickets;

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
