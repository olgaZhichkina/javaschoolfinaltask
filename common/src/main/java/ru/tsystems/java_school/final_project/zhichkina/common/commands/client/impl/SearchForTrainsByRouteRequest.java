package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import org.apache.commons.lang3.tuple.Pair;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.PassengerDaoCommand;

import java.util.Date;

/**
 * @author Olga Zhichkina
 */
public class SearchForTrainsByRouteRequest implements PassengerDaoCommand {
    private String departureStationName;
    private String arrivalStationName;
    private Pair<Date, Date> timeInterval;

    @Override
    public String operationName() {
        return "searchForTrainsByRoute";
    }

    public String getDepartureStationName() {
        return departureStationName;
    }

    public void setDepartureStationName(String departureStationName) {
        this.departureStationName = departureStationName;
    }

    public String getArrivalStationName() {
        return arrivalStationName;
    }

    public void setArrivalStationName(String arrivalStationName) {
        this.arrivalStationName = arrivalStationName;
    }

    public Pair<Date, Date> getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(Pair<Date, Date> timeInterval) {
        this.timeInterval = timeInterval;
    }
}
