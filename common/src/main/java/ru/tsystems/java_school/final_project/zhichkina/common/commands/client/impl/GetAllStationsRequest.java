package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.PassengerDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class GetAllStationsRequest implements PassengerDaoCommand {
    @Override
    public String operationName() {
        return "getAllStations";
    }
}
