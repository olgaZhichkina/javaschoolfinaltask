package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.PassengerDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class UserLoginRequest implements PassengerDaoCommand {
    String login;
    String password;

    @Override
    public String operationName() {
        return "userLogin";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
