package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import org.apache.commons.lang3.tuple.Pair;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;

import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class SearchForTrainsByStationResponse implements Command {
    private List<Pair<TimeTableLine, TimeTableLine>> lines;

    public List<Pair<TimeTableLine, TimeTableLine>> getLines() {
        return lines;
    }

    public void setLines(List<Pair<TimeTableLine, TimeTableLine>> lines) {
        this.lines = lines;
    }
}
