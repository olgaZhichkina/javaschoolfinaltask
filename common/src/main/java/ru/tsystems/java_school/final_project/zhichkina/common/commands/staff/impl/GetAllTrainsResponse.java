package ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;

import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class GetAllTrainsResponse implements Command {
    private List<String> names;

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }
}
