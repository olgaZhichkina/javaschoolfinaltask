package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;

import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class GetAllStationsResponse implements Command {
    List<String> stationNames;

    public List<String> getStationNames() {
        return stationNames;
    }

    public void setStationNames(List<String> stationNames) {
        this.stationNames = stationNames;
    }
}
