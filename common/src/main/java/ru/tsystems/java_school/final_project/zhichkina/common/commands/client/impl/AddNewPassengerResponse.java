package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;

/**
 * @author Olga Zhichkina
 */
public class AddNewPassengerResponse implements Command {
    boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
