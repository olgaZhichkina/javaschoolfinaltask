package ru.tsystems.java_school.final_project.zhichkina.common.utils;

import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Olga Zhichkina
 */
public class DateUtils {
    private static Logger log = Logger.getLogger(DateUtils.class);

    public static Date parse(String format, String source) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = formatter.parse(source);
        } catch (ParseException e) {
            log.error(e);
        }

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.YEAR, 2014);
        calendar.set(Calendar.MONTH, Calendar.AUGUST);
        calendar.set(Calendar.DAY_OF_MONTH, 25);
        return calendar.getTime();
    }

    public static Date parse(String source) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            return formatter.parse(source);
        } catch (ParseException e) {
            log.error(e);
            throw e;
        }
    }

    public static String format(String format, Date source) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(source);
    }

    public static Calendar getCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2014, Calendar.AUGUST, 25, 0, 0, 0);
        return calendar;
    }

    public static Calendar getNow() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2014, Calendar.AUGUST, 25);
        return calendar;
    }
}
