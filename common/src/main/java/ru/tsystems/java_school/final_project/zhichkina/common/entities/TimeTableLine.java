package ru.tsystems.java_school.final_project.zhichkina.common.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Entity
@Table(name = "time_table")
public class TimeTableLine implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "station_id")
    private Long stationId;

    @ManyToOne
    @JoinColumn(name = "train_id")
    private Train train;

    @Column(name = "time")
    private Date time;

    @OneToMany(mappedBy = "departure")
    private List<Ticket> departureTickets;

    @OneToMany(mappedBy = "arrival")
    private List<Ticket> arrivalTickets;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public List<Ticket> getDepartureTickets() {
        return departureTickets;
    }

    public void setDepartureTickets(List<Ticket> departureTickets) {
        this.departureTickets = departureTickets;
    }

    public List<Ticket> getArrivalTickets() {
        return arrivalTickets;
    }

    public void setArrivalTickets(List<Ticket> arrivalTickets) {
        this.arrivalTickets = arrivalTickets;
    }
}
