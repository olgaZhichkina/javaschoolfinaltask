package ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.StaffDaoCommand;

import java.util.Date;

/**
 * @author Olga Zhichkina
 */
public class AddTrainRequest implements StaffDaoCommand {
    String name;
    int numberOfSeats;
    String[] stationNames;
    Date[] times;

    @Override
    public String operationName() {
        return "addTrain";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public String[] getStationNames() {
        return stationNames;
    }

    public void setStationNames(String[] stationNames) {
        this.stationNames = stationNames;
    }

    public Date[] getTimes() {
        return times;
    }

    public void setTimes(Date[] times) {
        this.times = times;
    }
}
