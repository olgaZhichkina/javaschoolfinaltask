package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.PassengerDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class BuyTicketRequest implements PassengerDaoCommand {
    private long departureId;
    private long arrivalId;
    private long passengerId;

    @Override
    public String operationName() {
        return "buyTicket";
    }

    public long getDepartureId() {
        return departureId;
    }

    public void setDepartureId(long departureId) {
        this.departureId = departureId;
    }

    public long getArrivalId() {
        return arrivalId;
    }

    public void setArrivalId(long arrivalId) {
        this.arrivalId = arrivalId;
    }

    public long getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(long passengerId) {
        this.passengerId = passengerId;
    }
}
