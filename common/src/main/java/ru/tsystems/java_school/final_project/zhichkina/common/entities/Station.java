package ru.tsystems.java_school.final_project.zhichkina.common.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Entity
@Table(name = "stations")
public class Station implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "stations")
    private List<Train> trains;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Train> getTrains() {
        return trains;
    }

    public void setTrains(List<Train> trains) {
        this.trains = trains;
    }
}
