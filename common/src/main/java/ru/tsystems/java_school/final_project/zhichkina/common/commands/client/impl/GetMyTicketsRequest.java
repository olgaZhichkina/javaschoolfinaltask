package ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.PassengerDaoCommand;

/**
 * @author Olga Zhichkina
 */
public class GetMyTicketsRequest implements PassengerDaoCommand {
    long passenger_id;
    @Override
    public String operationName() {
        return "getMyTickets";
    }

    public long getPassenger_id() {
        return passenger_id;
    }

    public void setPassenger_id(long passenger_id) {
        this.passenger_id = passenger_id;
    }
}
