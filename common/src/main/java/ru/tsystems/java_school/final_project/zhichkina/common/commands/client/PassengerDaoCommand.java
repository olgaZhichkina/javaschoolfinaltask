package ru.tsystems.java_school.final_project.zhichkina.common.commands.client;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.RequestCommand;

/**
 * @author Olga Zhichkina
 */
public interface PassengerDaoCommand extends RequestCommand {
}
