package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import ru.tsystems.java_school.final_project.zhichkina.client.Client;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.GetAllStationsRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.GetAllStationsResponse;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.AddTrainRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.AddTrainResponse;
import ru.tsystems.java_school.final_project.zhichkina.common.utils.DateUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class WizardFrame extends JFrame {
    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 400;
    private JLabel nameLabel;
    private JLabel numOfSeatsLabel;
    private JTextField name;
    private JTextField numOfSeats;
    private JLabel stationLabel;
    private JLabel timeLabel;
    private JComboBox<String> stationOne;
    private JComboBox<String> stationTwo;
    private JTextField timeOne;
    private JTextField timeTwo;
    //private JButton moreButton;
    private JButton addButton;

    public WizardFrame() {

        setTitle("Add Train Wizard");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());

        nameLabel = new JLabel("Train Name");
        name = new JTextField(20);

        numOfSeatsLabel = new JLabel("Number Of Seats");
        numOfSeats = new JTextField(20);

        stationLabel = new JLabel("Station Name");
        timeLabel = new JLabel("Time (in HH:mm format)");

        stationOne = new JComboBox<>();
        stationTwo = new JComboBox<>();
        GetAllStationsResponse response = (GetAllStationsResponse) Client.execute(new GetAllStationsRequest());
        List<String> stations = response.getStationNames();
        for (String station : stations) {
            stationOne.addItem(station);
            stationTwo.addItem(station);
        }
        timeOne = new JTextField();
        timeTwo = new JTextField(); //TODO check regex dd:dd


       // moreButton = new JButton("One More Station");
        //moreButton.setEnabled(false);
/*        moreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });*/
        addButton = new JButton("Add");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddTrainRequest request = new AddTrainRequest();
                request.setName(name.getText());
                Integer.parseInt(numOfSeats.getText());
                int number = Integer.parseInt(numOfSeats.getText());
                request.setNumberOfSeats(number); //TODO numOfSeats.getText()
                request.setStationNames(new String[]{(String) stationOne.getSelectedItem(), (String) stationTwo.getSelectedItem()}); //TODO if more
                request.setTimes(new Date[]{DateUtils.parse("HH:mm", timeOne.getText()), DateUtils.parse("HH:mm", timeTwo.getText())});
                AddTrainResponse response = (AddTrainResponse) Client.execute(request);
                if (response.isResult()) {
                    JOptionPane.showMessageDialog(addButton, "The new train was successfully added!");
                } else {
                    JOptionPane.showMessageDialog(addButton, "The train was not added. Sorry!");
                }
                dispose();
            }
        });

        add(nameLabel, new GBC(0, 0).setAnchor(GBC.EAST).setInsets(5));
        add(name, new GBC(1, 0).setFill(GBC.WEST).setWeight(100, 0).setInsets(5));
        add(numOfSeatsLabel, new GBC(0, 1).setAnchor(GBC.EAST).setInsets(5));
        add(numOfSeats, new GBC(1, 1).setFill(GBC.WEST).setWeight(100, 0).setInsets(5));
        add(stationLabel, new GBC(0, 2).setAnchor(GBC.CENTER).setInsets(5));
        add(timeLabel, new GBC(1, 2).setAnchor(GBC.CENTER).setInsets(5));
        add(stationOne, new GBC(0, 3).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        add(stationTwo, new GBC(0, 4).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        add(timeOne, new GBC(1, 3).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        add(timeTwo, new GBC(1, 4).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        //add(moreButton, new GBC(0, 5).setInsets(3).setAnchor(GBC.CENTER));
        add(addButton, new GBC(1, 5).setInsets(3).setAnchor(GBC.CENTER));
    }
}
