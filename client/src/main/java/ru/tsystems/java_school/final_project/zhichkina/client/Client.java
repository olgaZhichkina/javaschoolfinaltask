package ru.tsystems.java_school.final_project.zhichkina.client;

import org.apache.log4j.Logger;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.RequestCommand;

import java.io.*;
import java.net.Socket;

/**
 * @author Olga Zhichkina
 */
public class Client {
    private static Logger log = Logger.getLogger(Client.class);
    //TODO create properties file
    private static final String HOST = "localhost";
    private static final int PORT = 8080;

    public static Command execute(RequestCommand requestCommand) {
        Command responseCommand = null;
        try (Socket sock = new Socket(HOST, PORT)) {
            log.debug("Connected to 8080 at localhost");
            log.debug("Local socket address is " + sock.getLocalSocketAddress());

            send(sock.getOutputStream(), requestCommand);
            log.debug("Successfully sent package to server");
            responseCommand = receive(sock.getInputStream());
            log.debug("Successfully received package from server");
        } catch (IOException | ClassNotFoundException e) {
            log.error("Some error occurred", e);
        }
        return responseCommand;
    }

    private static void send(OutputStream stream, RequestCommand m) throws IOException {
        ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(stream));
        output.writeObject(m);
        output.flush();
    }

    private static Command receive(InputStream stream) throws IOException, ClassNotFoundException {
        ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(stream));
        return (Command) input.readObject();
    }

}
