package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import ru.tsystems.java_school.final_project.zhichkina.client.Client;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.AddNewPassengerRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.AddNewPassengerResponse;
import ru.tsystems.java_school.final_project.zhichkina.common.utils.DateUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Date;

/**
 * @author Olga Zhichkina
 */
public class RegistrationFrame extends JFrame {
    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 200;
    private JLabel fNameLabel;
    private JTextField fName;
    private JLabel lNameLabel;
    private JTextField lName;
    private JLabel dBirthLabel;
    private JTextField dBirth; //TODO calender
    private JLabel loginLabel;
    private JTextField login;
    private JLabel passwordLabel;
    private JPasswordField password;
    private JButton registrationButton;

    public RegistrationFrame() {

        setTitle("Registration");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());

        fNameLabel = new JLabel("First Name");
        fName = new JTextField(20);
        lNameLabel = new JLabel("Last Name");
        lName = new JTextField(20);
        dBirthLabel = new JLabel("Date Of Birth (in dd.MM.yyyy format)");
        dBirth = new JTextField(20);
        loginLabel = new JLabel("Login");
        login = new JTextField(20);
        passwordLabel = new JLabel("Password");
        password = new JPasswordField(20);

        registrationButton = new JButton("Register");
        registrationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddNewPassengerRequest request = new AddNewPassengerRequest();
                request.setFirstName(fName.getText());
                request.setLastName(lName.getText());
                request.setLogin(login.getText());
                request.setPassword(new String(password.getPassword()));
                try {
                    request.setBirthDate(DateUtils.parse(dBirth.getText()));
                    AddNewPassengerResponse response = (AddNewPassengerResponse) Client.execute(request);
                    if (response.isResult()) {
                        JOptionPane.showMessageDialog(registrationButton, "You are successfully registered!");
                    } else {
                        JOptionPane.showMessageDialog(registrationButton, "We cannot register you. Sorry!");
                    }
                    dispose();
                } catch (ParseException exception) {
                    JOptionPane.showMessageDialog(registrationButton, "Wrong Date format, please correct it!");
                }
            }
        });

        add(fNameLabel, new GBC(0, 0).setAnchor(GBC.EAST).setInsets(5));
        add(fName, new GBC(1, 0).setFill(GBC.BOTH).setWeight(100, 0).setInsets(5));
        add(lNameLabel, new GBC(0, 1).setAnchor(GBC.EAST).setInsets(5));
        add(lName, new GBC(1, 1).setFill(GBC.BOTH).setWeight(100, 0).setInsets(5));
        add(dBirthLabel, new GBC(0, 2).setAnchor(GBC.EAST).setInsets(5));
        add(dBirth, new GBC(1, 2).setFill(GBC.BOTH).setWeight(100, 0).setInsets(5));
        add(loginLabel, new GBC(0, 3).setAnchor(GBC.EAST).setInsets(5));
        add(login, new GBC(1, 3).setFill(GBC.BOTH).setWeight(100, 0).setInsets(5));
        add(passwordLabel, new GBC(0, 4).setAnchor(GBC.EAST).setInsets(5));
        add(password, new GBC(1, 4).setFill(GBC.BOTH).setWeight(100, 0).setInsets(5));
        add(registrationButton, new GBC(1, 5).setInsets(3).setAnchor(GBC.CENTER));
    }
}
