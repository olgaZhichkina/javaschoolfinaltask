package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import ru.tsystems.java_school.final_project.zhichkina.client.Client;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.AddStationRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.AddStationResponse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author Olga Zhichkina
 */
public class AddStationFrame extends JFrame {
    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 100;
    private JLabel nameLabel;
    private JTextField name;
    private JButton addButton;

    public AddStationFrame() {

        setTitle("Add Station");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());

        nameLabel = new JLabel("Station Name");
        name = new JTextField(20);

        addButton = new JButton("Add");

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                AddStationRequest request = new AddStationRequest();
                request.setName(name.getText());
                AddStationResponse response = (AddStationResponse) Client.execute(request);
                if (response.isResult()) {
                    JOptionPane.showMessageDialog(addButton, "Station was successfully added");
                } else {
                    JOptionPane.showMessageDialog(addButton, "Station was not added");
                }
                dispose();
            }
        });

        add(nameLabel, new GBC(0, 0).setAnchor(GBC.EAST).setInsets(5));
        add(name, new GBC(1, 0).setFill(GBC.WEST).setWeight(100, 0).setInsets(5));
        add(addButton, new GBC(1, 1).setInsets(3).setAnchor(GBC.CENTER));
    }
}
