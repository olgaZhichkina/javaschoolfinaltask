package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import ru.tsystems.java_school.final_project.zhichkina.client.Client;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.UserLoginRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.UserLoginResponse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Olga Zhichkina
 */
public class LoginFrame extends JFrame {
    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 200;
    private JLabel loginLabel;
    private JTextField login;
    private JLabel passwordLabel;
    private JPasswordField password;
    private JButton loginButton;
    private JButton registerButton;

    public LoginFrame() {

        setTitle("Login");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());

        loginLabel = new JLabel("Login");
        login = new JTextField(20);
        passwordLabel = new JLabel("Password");
        password = new JPasswordField(20);

        registerButton = new JButton("Register");
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegistrationFrame frame = new RegistrationFrame();
                frame.setVisible(true);
                frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            }
        });
        loginButton = new JButton("Login");
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserLoginRequest request = new UserLoginRequest();
                request.setLogin(login.getText());
                request.setPassword(new String(password.getPassword()));
                UserLoginResponse response = (UserLoginResponse) Client.execute(request);
                Long id = response.getResult();
                if (id != null) {
                    UserTabbedPane pane = new UserTabbedPane(id);
                    pane.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(loginButton, "Please, check whether the login and password are correct. \nIf you are not registered yet, please register.");
                }
                dispose();
            }
        });

        add(loginLabel, new GBC(0, 0).setAnchor(GBC.EAST).setInsets(5));
        add(login, new GBC(1, 0).setFill(GBC.WEST).setWeight(100, 0).setInsets(5));
        add(passwordLabel, new GBC(0, 1).setAnchor(GBC.EAST).setInsets(5));
        add(password, new GBC(1, 1).setFill(GBC.WEST).setWeight(100, 0).setInsets(5));
        add(loginButton, new GBC(1, 2).setInsets(3).setAnchor(GBC.CENTER));
        add(registerButton, new GBC(1, 3).setInsets(3).setAnchor(GBC.CENTER));
    }
}
