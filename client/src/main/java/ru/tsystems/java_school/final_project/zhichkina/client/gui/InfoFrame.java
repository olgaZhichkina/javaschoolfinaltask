package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import ru.tsystems.java_school.final_project.zhichkina.client.Client;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.GetAllTrainsRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.GetAllTrainsResponse;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.ViewTrainsInfoRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.ViewTrainsInfoResponse;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Passenger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;
import ru.tsystems.java_school.final_project.zhichkina.common.utils.DateUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class InfoFrame extends JFrame {
    public static final int DEFAULT_WIDTH = 1000;
    public static final int DEFAULT_HEIGHT = 200;
    private JLabel trainLabel;
    private JComboBox<String> train;
    private JButton showButton;
    private JLabel timeTableLabel;
    private JTable timeTable;
    private JLabel passengerLabel;
    private JTable passenger;
    private TableModel timeTableModel;
    private TableModel passengerModel;

    public InfoFrame() {
        setTitle("Information");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());

        trainLabel = new JLabel("Train");
        train = new JComboBox<>();
        train.setEditable(false);

        GetAllTrainsResponse getTrainsCommand = (GetAllTrainsResponse) Client.execute(new GetAllTrainsRequest());
        List<String> trains = getTrainsCommand.getNames();
        for (String trainName : trains) {
            train.addItem(trainName);
        }

        showButton = new JButton("Show Information");
        showButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ViewTrainsInfoRequest request = new ViewTrainsInfoRequest();
                String trainName = (String) train.getSelectedItem();
                request.setTrainName(trainName);
                List<TimeTableLine> lines = new ArrayList<TimeTableLine>();
                List<Passenger> passengers = new ArrayList<Passenger>();
                ViewTrainsInfoResponse response = (ViewTrainsInfoResponse) Client.execute(request);
                if (response != null) {
                    lines = response.getLines();
                    passengers = response.getPassengers();
                }

                if (lines.size() > 0) {
                    Object[][] stationInfo = new Object[lines.size()][2];

                    int i = 0;
                    for (TimeTableLine line : lines) {
                        stationInfo[i][0] = response.getStationNames().get(i);
                        stationInfo[i][1] = DateUtils.format("HH:mm", line.getTime());
                        i++;
                    }
                    String[] stationInfoColumns = new String[]{"Station", "Time"};
                    timeTableModel = new DefaultTableModel(stationInfo, stationInfoColumns) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };

                    timeTable.setModel(timeTableModel);
                }


                if (passengers.size() > 0) {
                    Object[][] passengerInfo = new Object[passengers.size()][3];
                    int j = 0;
                    for (Passenger passenger : passengers) {
                        passengerInfo[j][0] = passenger.getFirstName();
                        passengerInfo[j][1] = passenger.getLastName();
                        passengerInfo[j][2] = DateUtils.format("dd.MM.yyyy", passenger.getDateOfBirth());
                        j++;
                    }
                    String[] passengerInfoColumns = new String[]{"Firs tName", "Last Name", "Date Of Birth"};
                    passengerModel = new DefaultTableModel(passengerInfo, passengerInfoColumns) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    passenger.setModel(passengerModel);
                }
            }
        });

        timeTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        passengerModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };


        timeTableLabel = new JLabel("Route");
        timeTable = new JTable(timeTableModel);
        passengerLabel = new JLabel("Passengers");
        passenger = new JTable(passengerModel);

        add(trainLabel, new GBC(0, 0).setAnchor(GBC.CENTER).setInsets(5));
        add(train, new GBC(0, 1).setFill(GBC.CENTER).setInsets(5));
        add(showButton, new GBC(0, 2).setInsets(3).setAnchor(GBC.CENTER));
        add(timeTableLabel, new GBC(1, 0).setAnchor(GBC.CENTER).setInsets(5));
        add(timeTable.getTableHeader(), new GBC(1, 1).setWeight(100, 0).setInsets(10).setFill(GBC.BOTH));
        add(timeTable, new GBC(1, 2).setWeight(100, 0).setInsets(10).setFill(GBC.BOTH));
        add(passengerLabel, new GBC(2, 0).setAnchor(GBC.CENTER).setInsets(10));
        add(passenger.getTableHeader(), new GBC(2, 1).setWeight(100, 0).setInsets(5).setFill(GBC.BOTH));
        add(passenger, new GBC(2, 2).setWeight(100, 0).setInsets(10).setFill(GBC.BOTH));
    }
}
