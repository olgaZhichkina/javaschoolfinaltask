package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import ru.tsystems.java_school.final_project.zhichkina.client.Client;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.*;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.CheckRouteRequest;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.CheckRouteResponse;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Passenger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Ticket;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Train;
import ru.tsystems.java_school.final_project.zhichkina.common.utils.DateUtils;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class UserTabbedPane extends JFrame {
    private long userId;
    public static final int DEFAULT_WIDTH = 800;
    public static final int DEFAULT_HEIGHT = 400;
    public static final int DEFAULT_TAB_WIDTH = 700;
    public static final int DEFAULT_TAB_HEIGHT = 300;
    public static final int DEFAULT_PANEL_WIDTH = 600;
    public static final int DEFAULT_PANEL_HEIGHT = 200;
    private JTabbedPane tabbedPane;
    private JComponent ticketsPanel;
    private JComponent myTicketsPanel;
    private JLabel titleLabel;
    private JLabel fromStationLabel;
    private JComboBox<String> fromStation;
    private JLabel toStationLabel;
    private JComboBox<String> toStation;
    private JLabel fromTimeLabel;
    private JComboBox<String> fromTime;
    private JLabel toTimeLabel;
    private JComboBox<String> toTime;
    private JButton searchButton;
    private JTable result;
    private TableModel resultModel;
    private JTextArea info;
    private JButton buyButton;
    private JButton checkRouteButton;
    private JButton addNewStationButton;
    private JButton addNewTrainButton;
    private JButton viewTrainsButton;
    private JButton refresh;
    private JTable myTicketsTable;
    private TableModel myTicketsTableModel;
    private List<Pair<TimeTableLine, TimeTableLine>> lines;


    public UserTabbedPane(long id) {
        this.userId = id;
        setTitle("Railroad Ticket Service");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

//---------------TICKET PURCHASE
        titleLabel = new JLabel("Choose Your Route");
        fromStationLabel = new JLabel("Station of departure");
        fromStation = new JComboBox();
        fromStation.setEditable(false);
        toStationLabel = new JLabel("Station of arrival");
        toStation = new JComboBox();
        toStation.setEditable(false);
        fromTimeLabel = new JLabel("From");
        fromTime = new JComboBox();
        fromTime.setEditable(false);
        toTimeLabel = new JLabel("To");
        toTime = new JComboBox();
        toTime.setEditable(false);
        GetAllStationsResponse response = (GetAllStationsResponse) Client.execute(new GetAllStationsRequest());
        List<String> stations = response.getStationNames();
        toStation.addItem("not selected");
        for (String station : stations) {
            fromStation.addItem(station);
            toStation.addItem(station);
        }

        Calendar calendar = DateUtils.getCalendar();
        Format formatter = new SimpleDateFormat("HH:mm");
        for (int t = 0; t < 24; t++) {
            fromTime.addItem(formatter.format(calendar.getTime()));
            toTime.addItem(formatter.format(calendar.getTime()));
            calendar.add(Calendar.HOUR, 1);
        }

        resultModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        result = new JTable(resultModel);
        searchButton = new JButton("Search");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buyButton.setEnabled(false);
                checkRouteButton.setEnabled(false);
                final String DATE_FORMAT = "HH:mm";
                Pair<Date, Date> timeInterval = new ImmutablePair<Date, Date>(DateUtils.parse(DATE_FORMAT, (String) fromTime.getSelectedItem()),
                        DateUtils.parse(DATE_FORMAT, (String) toTime.getSelectedItem()));

                if (toStation.getSelectedItem().equals("not selected")) {
                    SearchForTrainsByStationRequest request = new SearchForTrainsByStationRequest();
                    request.setStationName((String) fromStation.getSelectedItem());
                    SearchForTrainsByStationResponse response = (SearchForTrainsByStationResponse) Client.execute(request);

                    lines = response.getLines();
                } else {
                    SearchForTrainsByRouteRequest request = new SearchForTrainsByRouteRequest();
                    request.setDepartureStationName((String) fromStation.getSelectedItem());
                    request.setArrivalStationName((String) toStation.getSelectedItem());
                    request.setTimeInterval(timeInterval);
                    SearchForTrainsByRouteResponse response = (SearchForTrainsByRouteResponse) Client.execute(request);

                    lines = response.getLines();
                }

                Object[][] info = new Object[lines.size()][7];
                int i = 0;
                for (Pair<TimeTableLine, TimeTableLine> route : lines) {
                    Train train = route.getLeft().getTrain();
                    info[i][0] = (i + 1);
                    info[i][1] = train.getName();
                    info[i][2] = fromStation.getSelectedItem();
                    info[i][3] = DateUtils.format(DATE_FORMAT, route.getLeft().getTime());

                    if (!toStation.getSelectedItem().equals("not selected"))
                        info[i][4] = toStation.getSelectedItem();
                    else
                        info[i][4] = "";

                    if (route.getRight() != null)
                        info[i][5] = DateUtils.format(DATE_FORMAT, route.getRight().getTime());
                    else
                        info[i][5] = "";
                    info[i][6] = train.getNumberOfFreeSeats();
                    i++;
                }

                String[] infoColumns = new String[]{"#", "Train Name", "From", "Time Of Departure", "To", "Time Of Arrival", "Free Seats"};
                resultModel = new DefaultTableModel(info, infoColumns) {
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };

                result.setModel(resultModel);

                result.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        buyButton.setEnabled(false);
                        if (result.getSelectedRow() != -1 && !toStation.getSelectedItem().equals("not selected")) {
                            int currentFreeSeats = (int) result.getModel().getValueAt(result.getSelectedRow(), 6);
                            String departureTime = (String) result.getModel().getValueAt(result.getSelectedRow(), 3);
                            Date departureDate = DateUtils.parse("HH:mm", departureTime);
                            Date now = DateUtils.getNow().getTime();

                            DateTime departureDateDT = new DateTime(departureDate);
                            DateTime nowDT = new DateTime(now);

                            if (currentFreeSeats > 0 && Minutes.minutesBetween(nowDT, departureDateDT).getMinutes() > 10) {
                                buyButton.setEnabled(true);

                            }
                            checkRouteButton.setEnabled(true);
                        }
                    }
                });
            }
        });

        info = new JTextArea(" In order to find the appropriate train, please select the departure station" +
                " \n and (optionally) arrival station and specify the time interval." +
                " \n In order to buy the ticket, please select the train and press the Buy button." +
                " \n If you want to see the entire route, please select the train" +
                " \n and press the Check Route button. You can only buy one ticket per train if " +
                " \n there are free seats and more than 10 minutes before the departure.");
        info.setOpaque(false);
        info.setEditable(false);

        buyButton = new JButton("Buy");
        buyButton.setEnabled(false);
        buyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = (Integer) result.getModel().getValueAt(result.getSelectedRow(), 0);
                Pair<TimeTableLine, TimeTableLine> route = lines.get(selectedRow - 1);

                TimeTableLine departure = route.getLeft();
                TimeTableLine arrival = route.getRight();

                BuyTicketRequest request = new BuyTicketRequest();
                request.setDepartureId(departure.getId());
                request.setArrivalId(arrival.getId());

                request.setPassengerId(userId);
                BuyTicketResponse response = (BuyTicketResponse) Client.execute(request);
                if (response.isResult()) {
                    JOptionPane.showMessageDialog(buyButton, "Done!");
                } else {
                    JOptionPane.showMessageDialog(buyButton, "You cannot buy two tickets for one train. Sorry!");

                }
            }
        });

        checkRouteButton = new JButton("Check Route");
        checkRouteButton.setEnabled(false);
        checkRouteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CheckRouteRequest request = new CheckRouteRequest();
                String trainName = (String) result.getModel().getValueAt(result.getSelectedRow(), 1);
                request.setTrainName(trainName);
                CheckRouteResponse checkRouteResponse = (CheckRouteResponse) Client.execute(request);
                List<TimeTableLine> lines = checkRouteResponse.getLines();
                Object[][] stationInfo = new Object[lines.size()][2];
                int i = 0;
                for (TimeTableLine line : lines) {
                    stationInfo[i][0] = checkRouteResponse.getStationNames().get(i);
                    stationInfo[i][1] = DateUtils.format("HH:mm", line.getTime());
                    i++;
                }
                String[] stationInfoColumns = new String[]{"Station", "Time"};
                RouteFrame routeFrame = new RouteFrame(stationInfo, stationInfoColumns);
                routeFrame.setVisible(true);
                routeFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            }
        });
        ticketsPanel = new JPanel();
        ticketsPanel.setSize(DEFAULT_PANEL_WIDTH, DEFAULT_PANEL_HEIGHT);
        ticketsPanel.setLayout(new GridBagLayout());
        ticketsPanel.add(titleLabel, new GBC(0, 0, 4, 1).setAnchor(GBC.CENTER).setInsets(10));
        ticketsPanel.add(fromStationLabel, new GBC(0, 1).setAnchor(GBC.EAST).setInsets(5));
        ticketsPanel.add(fromStation, new GBC(1, 1).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        ticketsPanel.add(toStationLabel, new GBC(2, 1).setAnchor(GBC.EAST).setInsets(5));
        ticketsPanel.add(toStation, new GBC(3, 1).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        ticketsPanel.add(fromTimeLabel, new GBC(0, 2).setAnchor(GBC.EAST).setInsets(5));
        ticketsPanel.add(fromTime, new GBC(1, 2).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        ticketsPanel.add(toTimeLabel, new GBC(2, 2).setAnchor(GBC.EAST).setInsets(5));
        ticketsPanel.add(toTime, new GBC(3, 2).setFill(GBC.HORIZONTAL).setWeight(100, 0).setInsets(5));
        ticketsPanel.add(searchButton, new GBC(3, 3).setInsets(3).setAnchor(GBC.EAST));
        ticketsPanel.add(new JScrollPane(result), new GBC(0, 4, 5, 1).setFill(GBC.BOTH).setWeight(100, 100).setInsets(10));
        ticketsPanel.add(new JScrollPane(info), new GBC(0, 5, 3, 3).setFill(GBC.BOTH).setWeight(100, 100).setInsets(10));
        ticketsPanel.add(checkRouteButton, new GBC(3, 5).setInsets(3).setAnchor(GBC.CENTER));
        ticketsPanel.add(buyButton, new GBC(3, 6).setInsets(3).setAnchor(GBC.CENTER));

//-------------MY TICKETS

        myTicketsPanel = new JPanel();
        myTicketsPanel.setSize(DEFAULT_PANEL_WIDTH, DEFAULT_PANEL_HEIGHT);
        myTicketsPanel.setLayout(new GridBagLayout());
        myTicketsTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        myTicketsTable = new JTable(myTicketsTableModel);
        refresh = new JButton("Refresh");
        refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GetMyTicketsRequest getMyTicketsRequest = new GetMyTicketsRequest();
                getMyTicketsRequest.setPassenger_id(userId);
                GetMyTicketsResponse getMyTicketsResponse = (GetMyTicketsResponse) Client.execute(getMyTicketsRequest);
                List<Ticket> myTickets = getMyTicketsResponse.getTickets();
                if (myTickets.size() > 0) {
                    Object[][] ticketsInfo = new Object[myTickets.size()][3];
                    int j = 0;
                    for (Ticket ticket : myTickets) {
                        ticketsInfo[j][0] = ticket.getId();
                        ticketsInfo[j][1] = DateUtils.format("dd.MM.yyyy", ticket.getDateOfPurchase());
                        ticketsInfo[j][2] = ticket.getTrainId();
                        j++;
                    }
                    String[] ticketsInfoColumns = new String[]{"#", "Date Of Purchase", "Train"};
                    myTicketsTableModel = new DefaultTableModel(ticketsInfo, ticketsInfoColumns) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    myTicketsTable.setModel(myTicketsTableModel);
                }
            }
        });
        myTicketsPanel.add(myTicketsTable.getTableHeader(), new GBC(0, 0).setAnchor(GBC.CENTER).setInsets(5));
        myTicketsPanel.add(myTicketsTable, new GBC(0, 1).setAnchor(GBC.WEST).setInsets(5));
        myTicketsPanel.add(refresh,new GBC(0,2).setAnchor(GBC.CENTER).setInsets(5));
        GetMyTicketsRequest getMyTicketsRequest = new GetMyTicketsRequest();
        getMyTicketsRequest.setPassenger_id(userId);
        GetMyTicketsResponse getMyTicketsResponse = (GetMyTicketsResponse) Client.execute(getMyTicketsRequest);
        List<Ticket> myTickets = getMyTicketsResponse.getTickets();
        if (myTickets.size() > 0) {
            Object[][] ticketsInfo = new Object[myTickets.size()][3];
            int j = 0;
            for (Ticket ticket : myTickets) {
                ticketsInfo[j][0] = ticket.getId();
                ticketsInfo[j][1] = DateUtils.format("dd.MM.yyyy", ticket.getDateOfPurchase());
                ticketsInfo[j][2] = ticket.getTrainId();
                j++;
            }
            String[] ticketsInfoColumns = new String[]{"#", "Date Of Purchase", "Train"};
            myTicketsTableModel = new DefaultTableModel(ticketsInfo, ticketsInfoColumns) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            myTicketsTable.setModel(myTicketsTableModel);
        }
       // myTicketsPanel.addAncestorListener();

//-------------ADMINISTRATION
        JPanel administrationPanel = new JPanel();
        administrationPanel.setSize(DEFAULT_PANEL_WIDTH, DEFAULT_PANEL_HEIGHT);
        administrationPanel.setLayout(new GridBagLayout());
        addNewStationButton = new JButton("Add New Station");
        addNewStationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame addStationFrame = new AddStationFrame();
                addStationFrame.setVisible(true);
                addStationFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            }
        });

        addNewTrainButton = new JButton("Add New Train");
        addNewTrainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame addTrainFrame = new WizardFrame();
                addTrainFrame.setVisible(true);
                addTrainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            }
        });

        viewTrainsButton = new JButton("View Trains Info");
        viewTrainsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame viewTrainsFrame = new InfoFrame();
                viewTrainsFrame.setVisible(true);
                viewTrainsFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            }
        });

        administrationPanel.add(addNewStationButton, new GBC(0, 0).setInsets(3).setAnchor(GBC.NORTHWEST));
        administrationPanel.add(addNewTrainButton, new GBC(0, 1).setInsets(3).setAnchor(GBC.NORTHWEST));
        administrationPanel.add(viewTrainsButton, new GBC(0, 2).setInsets(3).setAnchor(GBC.NORTHWEST));
//----------------------------
        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setSize(DEFAULT_TAB_WIDTH, DEFAULT_TAB_HEIGHT);
        setContentPane(tabbedPane);
        tabbedPane.addTab("Ticket Purchase", ticketsPanel);
        tabbedPane.addTab("My Tickets", myTicketsPanel);
        GetRightsRequest request = new GetRightsRequest();
        request.setPassengerId(userId);
        GetRightsResponse rightsResponse = (GetRightsResponse) Client.execute(request);
        Integer role = rightsResponse.getResult();
        if (role.intValue() == 1) {
            tabbedPane.addTab("Administration", administrationPanel);
        }
    }
}
