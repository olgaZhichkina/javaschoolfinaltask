package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import javax.swing.*;
import java.awt.*;

/**
 * @author Olga Zhichkina
 */
public class FramesRun {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new LoginFrame();
//                JFrame frame = new RegistrationFrame();
//                JFrame frame = new AddStationFrame();
//                JFrame frame = new RouteFrame();
//                JFrame frame = new InfoFrame();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }
}
