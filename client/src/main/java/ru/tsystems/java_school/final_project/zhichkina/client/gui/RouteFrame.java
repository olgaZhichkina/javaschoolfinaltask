package ru.tsystems.java_school.final_project.zhichkina.client.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;

/**
 * @author Olga Zhichkina
 */
public class RouteFrame extends JFrame {
    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 200;
    private JTable route;
    private TableModel model;

    public RouteFrame(Object[][] data, String[] columnNames) {

        setTitle("Route");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());

        model = new DefaultTableModel(data, columnNames) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        route = new JTable(model);
        add(route.getTableHeader(), new GBC(0, 0).setAnchor(GBC.CENTER).setInsets(5));
        add(route, new GBC(0, 1).setAnchor(GBC.WEST).setInsets(5));
    }
}
