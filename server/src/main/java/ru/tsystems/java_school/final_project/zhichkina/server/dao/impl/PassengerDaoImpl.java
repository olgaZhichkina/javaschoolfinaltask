package ru.tsystems.java_school.final_project.zhichkina.server.dao.impl;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Passenger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Ticket;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Train;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.PassengerDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@SuppressWarnings({"unchecked", "JpaQlInspection", "JpaQueryApiInspection"})
public class PassengerDaoImpl implements PassengerDao {
    private Logger log = Logger.getLogger(PassengerDaoImpl.class);
    private EntityManagerFactory entityManagerFactory;

    public PassengerDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<Pair<TimeTableLine, TimeTableLine>> searchForTrainsByRoute(String departureStationName, String arrivalStationName,
                                                                           Pair<Date, Date> timeInterval) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Pair<TimeTableLine, TimeTableLine>> result = new ArrayList<>();

        try {
            Long departureStationId = findStationByName(departureStationName);
            Long arrivalStationId = findStationByName(arrivalStationName);
            List<TimeTableLine> lines = entityManager.createQuery("from TimeTableLine where stationId = :stationId and (time between :from and :till)")
                    .setParameter("stationId", departureStationId)
                    .setParameter("from", timeInterval.getLeft())
                    .setParameter("till", timeInterval.getRight())
                    .getResultList();

            //TODO rewrite to one query
            for (TimeTableLine line : lines) {
                TimeTableLine arrival = null;
                List list = entityManager.createQuery("from TimeTableLine where stationId = :stationId and train = :train")
                        .setParameter("stationId", arrivalStationId)
                        .setParameter("train", line.getTrain())
                        .getResultList();

                if (list.size() > 0)
                    arrival = (TimeTableLine) list.get(0);
                if (arrival != null) {
                    result.add(new ImmutablePair<>(line, arrival));
                }
            }
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public List<TimeTableLine> searchForTrainsByStation(String stationName) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<TimeTableLine> result = new ArrayList<>();
        try {
            Long stationId = findStationByName(stationName);
            result = entityManager.createQuery("from TimeTableLine where stationId = :stationId")
                    .setParameter("stationId", stationId)
                    .getResultList();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public boolean buyTicket(long departureId, long arrivalId, long passengerId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Ticket ticket = new Ticket();
        ticket.setDateOfPurchase(new Date());
        try {
            Passenger passenger = entityManager.find(Passenger.class, passengerId);
            TimeTableLine departure = entityManager.find(TimeTableLine.class, departureId);
            TimeTableLine arrival = entityManager.find(TimeTableLine.class, arrivalId);
            long trainId = departure.getTrain().getId();

            //check if already bought
            List<Ticket> tickets = entityManager.createQuery("from Ticket where passenger = :passenger AND trainId = :trainId")
                    .setParameter("passenger", passenger)
                    .setParameter("trainId", trainId)
                    .getResultList();
            if (tickets.size() != 0)
                return false;

            ticket.setPassenger(passenger);
            ticket.setDeparture(departure);
            ticket.setArrival(arrival);
            ticket.setTrainId(trainId);

            entityManager.getTransaction().begin();
            entityManager.persist(ticket);
            Train train = entityManager.find(Train.class, trainId);
            train.setNumberOfFreeSeats(train.getNumberOfFreeSeats() - 1);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public boolean addNewPassenger(String fistName, String lastName, Date birthDate, String login, String password) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Passenger passenger = new Passenger();
        passenger.setFirstName(fistName);
        passenger.setLastName(lastName);
        passenger.setDateOfBirth(birthDate);
        passenger.setLogin(login);
        passenger.setPassword(password);
        passenger.setRole(0);
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(passenger); //TODO check if there is no such passenger yet
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public List<String> getAllStations() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<String> result = new ArrayList<>();
        try {
            result = entityManager.createQuery("select name from Station")
                    .getResultList();
        } finally {
            entityManager.close();
        }
        return result;
    }

    private Long findStationByName(String name) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Long station = null;
        try {
            station = (Long) entityManager.createQuery("select id from Station where name = :name")
                    .setParameter("name", name)
                    .getSingleResult();
        } finally {
            entityManager.close();
        }
        return station;
    }

    @Override
    public Long userLogin(String login, String password) {
        Long result = null;
        if (login == null || password == null) return result;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<String> loginList = entityManager.createQuery("select login from Passenger").getResultList();
        if (loginList.contains(login)) {
            String pass = (String) entityManager.createQuery("select password from Passenger where login = :log").setParameter("log", login).getSingleResult();
            if (password.equals(pass)) {
                long passengerId = (long) entityManager.createQuery("select id from Passenger where login = :log").setParameter("log", login).getSingleResult();
                result = new Long(passengerId);
            }
        }
        return result;
    }

    @Override
    public Integer getRights(long passengerId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        int role = (int) entityManager.createQuery("select role from Passenger where id = :id").setParameter("id", passengerId).getSingleResult();
        return new Integer(role);
    }

    @Override
    public List<Ticket> getMyTickets(long passengerId) {
        List<Ticket> tickets = new ArrayList<>();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        tickets = entityManager.createQuery("from Ticket where passenger_id = :id").setParameter("id", passengerId).getResultList();
        return tickets;
    }
}
