package ru.tsystems.java_school.final_project.zhichkina.server;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 * @author Olga Zhichkina
 */
public class EmfHolder {
    @PersistenceContext
    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("railroad_test");

    private EmfHolder() {
    }

    public static EntityManagerFactory getInstance() {
        return entityManagerFactory;
    }

    public static void close() {
        if (entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
    }
}
