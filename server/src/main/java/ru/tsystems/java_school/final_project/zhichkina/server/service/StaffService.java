package ru.tsystems.java_school.final_project.zhichkina.server.service;

import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.RequestCommand;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.impl.*;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Passenger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;
import ru.tsystems.java_school.final_project.zhichkina.server.EmfHolder;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.StaffDao;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.impl.StaffDaoImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class StaffService {
    private static StaffDao dao = new StaffDaoImpl(EmfHolder.getInstance());

    public static Command dispatch(RequestCommand input) {
        Command result = null;
        if (input.operationName().equals("addStation")) {
            result = addStation((AddStationRequest) input);
        } else if (input.operationName().equals("addTrain")) {
            result = addTrain((AddTrainRequest) input);
        } else if (input.operationName().equals("viewTrainsInfo")) {
            result = viewTrainsInfo((ViewTrainsInfoRequest) input);
        } else if (input.operationName().equals("getAllTrains")) {
            result = getAllTrains((GetAllTrainsRequest) input);
        } else if (input.operationName().equals("checkRoute")) {
            result = checkRoute((CheckRouteRequest) input);
        }
        return result;
    }

    private static Command checkRoute(CheckRouteRequest request) {
        CheckRouteResponse result = new CheckRouteResponse();
        List<TimeTableLine> lines = dao.viewStationsInfo(request.getTrainName());
        result.setLines(lines);

        List<Long> ids = new ArrayList<>();
        for (TimeTableLine line : lines)
            ids.add(line.getStationId());
        List<String> stationNames = dao.findStationNamesByIds(ids);
        result.setStationNames(stationNames);
        return result;
    }

    private static Command addTrain(AddTrainRequest request) {
        AddTrainResponse result = new AddTrainResponse();
        boolean isAdded = dao.addTrain(request.getName(), request.getNumberOfSeats(),
                request.getStationNames(), request.getTimes());
        result.setResult(isAdded);
        return result;
    }

    private static AddStationResponse addStation(AddStationRequest request) {
        AddStationResponse result = new AddStationResponse();
        boolean isAdded = dao.addStation(request.getName());
        result.setResult(isAdded);
        return result;
    }

    private static ViewTrainsInfoResponse viewTrainsInfo(ViewTrainsInfoRequest request) {
        ViewTrainsInfoResponse result = new ViewTrainsInfoResponse();
        List<TimeTableLine> lines = new ArrayList<>();
        lines = dao.viewStationsInfo(request.getTrainName());
        result.setLines(lines);
        List<Passenger> passengers = new ArrayList<>();
        passengers = dao.viewPassengersInfo(request.getTrainName());
        result.setPassengers(passengers);

        List<Long> ids = new ArrayList<>();
        for (TimeTableLine line : lines)
            ids.add(line.getStationId());
        List<String> stationNames = dao.findStationNamesByIds(ids);
        result.setStationNames(stationNames);
        return result;
    }

    private static GetAllTrainsResponse getAllTrains(GetAllTrainsRequest request) {
        GetAllTrainsResponse response = new GetAllTrainsResponse();
        dao.getAllTrains();
        response.setNames(dao.getAllTrains());
        return response;
    }
}
