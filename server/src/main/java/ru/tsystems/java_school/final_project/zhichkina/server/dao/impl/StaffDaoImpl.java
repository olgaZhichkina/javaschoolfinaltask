package ru.tsystems.java_school.final_project.zhichkina.server.dao.impl;

import org.apache.log4j.Logger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.*;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.StaffDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@SuppressWarnings({"unchecked", "JpaQlInspection", "JpaQueryApiInspection"})
public class StaffDaoImpl implements StaffDao {

    private Logger log = Logger.getLogger(StaffDaoImpl.class);
    private EntityManagerFactory entityManagerFactory;

    public StaffDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public boolean addStation(String name) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Station station = new Station();
        station.setName(name);
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(station); //TODO check if there is no such station yet
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public boolean addTrain(String name, int numberOfSeats, String[] stationNames, Date[] times) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Train train = new Train();
        train.setName(name);
        train.setNumberOfSeats(numberOfSeats);
        train.setNumberOfFreeSeats(numberOfSeats);
        List<TimeTableLine> lines = new ArrayList<TimeTableLine>();

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(train);//TODO check if there is no such train yet
            for (int i = 0; i < stationNames.length; i++) {
                TimeTableLine line = new TimeTableLine();
                line.setStationId(findStationByName(stationNames[i]));
                line.setTime(times[i]);
                line.setTrain(train);
                lines.add(line);
                entityManager.persist(line);
            }
            train.setLines(lines);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public List<TimeTableLine> viewStationsInfo(String trainName) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<TimeTableLine> lines = new ArrayList<TimeTableLine>();
        try {
            Train train = (Train) entityManager.createQuery("from Train where name = :name")
                    .setParameter("name", trainName).getSingleResult();
            lines = train.getLines();
        } finally {
            entityManager.close();
        }
        return lines;
    }

    @Override
    public List<Passenger> viewPassengersInfo(String trainName) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Passenger> passengers = new ArrayList<Passenger>();
        try {
            Train train = (Train) entityManager.createQuery("from Train where name = :name")
                    .setParameter("name", trainName).getSingleResult();
            List<Ticket> tickets = entityManager.createQuery("from Ticket where trainId = :trainId")
                    .setParameter("trainId", train.getId())
                    .getResultList();
            for (Ticket t : tickets) {
                passengers.add(t.getPassenger());
            }
        } finally {
            entityManager.close();
        }
        return passengers;
    }

    @Override
    public List<String> getAllTrains() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<String> result = new ArrayList<>();
        try {
            result = entityManager.createQuery("select name from Train")
                    .getResultList();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public List<String> findStationNamesByIds(List<Long> ids) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<String> result = new ArrayList<>();
        try {
            result = entityManager.createQuery("select name from Station where id in :ids")
                    .setParameter("ids", ids)
                    .getResultList();
        } finally {
            entityManager.close();
        }
        return result;
    }

    //TODO: remove method duplication
    private Long findStationByName(String name) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Long station = null;
        try {
            station = (Long) entityManager.createQuery("select id from Station where name = :name")
                    .setParameter("name", name)
                    .getSingleResult();
        } finally {
            entityManager.close();
        }
        return station;
    }
}
