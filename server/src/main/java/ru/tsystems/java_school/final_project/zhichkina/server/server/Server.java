package ru.tsystems.java_school.final_project.zhichkina.server.server;

import org.apache.log4j.Logger;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.RequestCommand;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.PassengerDaoCommand;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.staff.StaffDaoCommand;
import ru.tsystems.java_school.final_project.zhichkina.server.EmfHolder;
import ru.tsystems.java_school.final_project.zhichkina.server.service.PassengerService;
import ru.tsystems.java_school.final_project.zhichkina.server.service.StaffService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Olga Zhichkina
 */
public class Server {
    private static Logger log = Logger.getLogger(Server.class);
    //TODO create properties file
    private static final int PORT = 8080;

    public static void main(String[] args) {
        try (ServerSocket serverSock = new ServerSocket(PORT)) {
            EmfHolder.getInstance();
            log.debug("Server is started!");
            while (true) {
                final Socket clientSock = serverSock.accept();
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Command command = receive(clientSock.getInputStream());
                            log.debug("Successfully received package from client");
                            send(clientSock.getOutputStream(), command);
                            log.debug("Successfully sent package to client");
                        } catch (Exception e) {
                            log.error(e);
                        } finally {
                            if (clientSock != null) {
                                try {
                                    clientSock.close();
                                } catch (IOException e) {
                                    log.error(e);
                                }
                            }
                        }
                    }
                }).start();
            }
        } catch (IOException e) {
            log.error(e);
        }
    }

    private static void send(OutputStream stream, Command m) throws IOException {
        ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(stream));
        output.writeObject(m);
        output.flush();
    }

    private static Command receive(InputStream stream) throws IOException, ClassNotFoundException {
        ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(stream));
        RequestCommand m = (RequestCommand) input.readObject();
        log.debug("Receiving command from client");
        Command result = null;
        if (m instanceof PassengerDaoCommand) {
            log.debug("Client sends PassengerCommand. Invocation of a Passenger service.");
            result = PassengerService.dispatch(m);
        } else if (m instanceof StaffDaoCommand) {
            log.debug("Client sends StaffCommand. Invocation of a Staff service.");
            result = StaffService.dispatch(m);
        }
        return result;
    }

}
