package ru.tsystems.java_school.final_project.zhichkina.server.service;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.Command;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.RequestCommand;
import ru.tsystems.java_school.final_project.zhichkina.common.commands.client.impl.*;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Ticket;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;
import ru.tsystems.java_school.final_project.zhichkina.server.EmfHolder;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.PassengerDao;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.impl.PassengerDaoImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class PassengerService {
    private static PassengerDao dao = new PassengerDaoImpl(EmfHolder.getInstance());

    public static Command dispatch(RequestCommand input) {
        Command result = null;
        if (input.operationName().equals("searchForTrainsByRoute")) {
            result = searchForTrainsByRoute((SearchForTrainsByRouteRequest) input);
        } else if (input.operationName().equals("searchForTrainsByStation")) {
            result = searchForTrainsByStation((SearchForTrainsByStationRequest) input);
        } else if (input.operationName().equals("buyTicket")) {
            result = buyTicket((BuyTicketRequest) input);
        } else if (input.operationName().equals("addNewPassenger")) {
            result = addNewPassenger((AddNewPassengerRequest) input);
        } else if (input.operationName().equals("getAllStations")) {
            result = getAllStations((GetAllStationsRequest) input);
        } else if (input.operationName().equals("userLogin")) {
            result = userLogin((UserLoginRequest) input);
        } else if (input.operationName().equals("getRights")) {
            result = getRights((GetRightsRequest) input);
        } else if (input.operationName().equals("getMyTickets")) {
            result = getMyTickets((GetMyTicketsRequest) input);
        }
        return result;
    }

    private static Command getMyTickets(GetMyTicketsRequest request) {
        GetMyTicketsResponse response = new GetMyTicketsResponse();
        List<Ticket> tickets = dao.getMyTickets(request.getPassenger_id());
        response.setTickets(tickets);
        return response;
    }

    private static SearchForTrainsByStationResponse searchForTrainsByStation(SearchForTrainsByStationRequest request) {
        SearchForTrainsByStationResponse response = new SearchForTrainsByStationResponse();
        List<TimeTableLine> lines = dao.searchForTrainsByStation(request.getStationName());
        List<Pair<TimeTableLine, TimeTableLine>> pairs = new ArrayList<>();
        for (TimeTableLine line : lines) {
            pairs.add(new ImmutablePair<TimeTableLine, TimeTableLine>(line, null));
        }
        response.setLines(pairs);

        return response;
    }

    private static BuyTicketResponse buyTicket(BuyTicketRequest request) {
        BuyTicketResponse response = new BuyTicketResponse();
        boolean result = dao.buyTicket(request.getDepartureId(), request.getArrivalId(), request.getPassengerId());
        response.setResult(result);
        return response;
    }

    private static Command addNewPassenger(AddNewPassengerRequest request) {
        AddNewPassengerResponse result = new AddNewPassengerResponse();
        boolean isAdded = dao.addNewPassenger(request.getFirstName(), request.getLastName(),
                request.getBirthDate(), request.getLogin(), request.getPassword());
        result.setResult(isAdded);
        return result;
    }

    private static Command getRights(GetRightsRequest request) {
        GetRightsResponse result = new GetRightsResponse();
        Integer role = dao.getRights(request.getPassengerId());
        result.setResult(role);
        return result;
    }

    private static Command userLogin(UserLoginRequest request) {
        UserLoginResponse result = new UserLoginResponse();
        Long id = dao.userLogin(request.getLogin(), request.getPassword());
        result.setResult(id);
        return result;
    }

    private static GetAllStationsResponse getAllStations(GetAllStationsRequest request) {
        GetAllStationsResponse result = new GetAllStationsResponse();
        //dao.getAllStations();
        List<String> stations = dao.getAllStations();
        result.setStationNames(stations);
        return result;
    }

    private static SearchForTrainsByRouteResponse searchForTrainsByRoute(SearchForTrainsByRouteRequest request) {
        SearchForTrainsByRouteResponse result = new SearchForTrainsByRouteResponse();
        List<Pair<TimeTableLine, TimeTableLine>> lines = dao.searchForTrainsByRoute(request.getDepartureStationName(),
                request.getArrivalStationName(),
                request.getTimeInterval());
        result.setLines(lines);
        return result;
    }
}
