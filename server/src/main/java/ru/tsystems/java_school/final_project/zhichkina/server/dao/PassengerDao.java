package ru.tsystems.java_school.final_project.zhichkina.server.dao;

import org.apache.commons.lang3.tuple.Pair;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Ticket;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;

import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public interface PassengerDao {

    List<Pair<TimeTableLine, TimeTableLine>> searchForTrainsByRoute(String departureStationName, String arrivalStationName,
                                                                    Pair<Date, Date> timeInterval);

    List<TimeTableLine> searchForTrainsByStation(String stationName);

    boolean buyTicket(long departureId, long arrivalId, long passengerId);

    boolean addNewPassenger(String fistName, String lastName, Date birthDate, String login, String password);

    List<String> getAllStations();

    Long userLogin(String login, String password);

    Integer getRights(long passengerId);

    List<Ticket> getMyTickets(long passengerId);
}
