package ru.tsystems.java_school.final_project.zhichkina.server.dao;

import ru.tsystems.java_school.final_project.zhichkina.common.entities.Passenger;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;

import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public interface StaffDao {

    boolean addStation(String name);

    boolean addTrain(String name, int numberOfSeats, String[] stationNames, Date[] times);

    List<TimeTableLine> viewStationsInfo(String trainName);

    List<Passenger> viewPassengersInfo(String trainName);

    List<String> getAllTrains();

    List<String> findStationNamesByIds(List<Long> ids);
}
