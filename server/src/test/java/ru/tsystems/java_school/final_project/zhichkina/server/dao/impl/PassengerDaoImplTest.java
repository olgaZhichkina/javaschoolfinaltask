package ru.tsystems.java_school.final_project.zhichkina.server.dao.impl;

import junit.framework.TestCase;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.*;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.TimeTableLine;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Train;
import ru.tsystems.java_school.final_project.zhichkina.server.EmfHolder;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.PassengerDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class PassengerDaoImplTest {

    private static PassengerDao dao;

    @BeforeClass
    public static void init() {
        dao = new PassengerDaoImpl(EmfHolder.getInstance());
    }

    @AfterClass
    public static void destroy() {
        EmfHolder.close();
    }

    @Test
    public void searchForTrainsByRouteTest() throws ParseException {
        String format = "HH:mm:ss y-M-d";
        String from = "10:00:00 2014-08-25";
        String to = "14:00:00 2014-08-25";
        SimpleDateFormat formatter = new SimpleDateFormat(format);

        List<Pair<TimeTableLine, TimeTableLine>> route = dao.searchForTrainsByRoute("Moscow", "Berlin",
                new ImmutablePair<>(formatter.parse(from), formatter.parse(to)));
        Train first = route.get(0).getLeft().getTrain();
        Train second = route.get(1).getLeft().getTrain();
        TestCase.assertEquals("VBB", first.getName());
        TestCase.assertEquals("ICE", second.getName());
    }

    @Test
    public void searchForTrainsByStationTest() {
        List<TimeTableLine> route = dao.searchForTrainsByStation("Berlin");
        Train first = route.get(0).getTrain();
        Train second = route.get(1).getTrain();
        TestCase.assertEquals("VBB", first.getName());
        TestCase.assertEquals("ICE", second.getName());
    }

    @Test
    public void buyTicketTest() {
        boolean result = dao.buyTicket(3, 5, 1);
        TestCase.assertTrue(result);
    }

    @Test
    public void addNewPassengerTest() throws ParseException {
        String format = "y-M-d HH:mm:ss";
        String birthDate = "1989-01-20 00:00:00";
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        boolean result = dao.addNewPassenger("Darya", "Goldman", formatter.parse(birthDate), "darya", "darya");
        TestCase.assertTrue(result);
    }

}
