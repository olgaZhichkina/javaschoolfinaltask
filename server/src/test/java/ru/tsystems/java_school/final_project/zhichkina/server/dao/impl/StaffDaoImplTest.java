package ru.tsystems.java_school.final_project.zhichkina.server.dao.impl;

import junit.framework.TestCase;
import org.junit.*;
import ru.tsystems.java_school.final_project.zhichkina.common.entities.Passenger;
import ru.tsystems.java_school.final_project.zhichkina.common.utils.DateUtils;
import ru.tsystems.java_school.final_project.zhichkina.server.EmfHolder;
import ru.tsystems.java_school.final_project.zhichkina.server.dao.StaffDao;

import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public class StaffDaoImplTest {
    private static StaffDao dao;

    @BeforeClass
    public static void init() {
        dao = new StaffDaoImpl(EmfHolder.getInstance());
    }

    @AfterClass
    public static void destroy() {
        EmfHolder.close();
    }

    @Test
    public void addStationTest() {

        TestCase.assertTrue(dao.addStation("Murmansk"));
    }

    @Test
    public void addTrainTest() {
        dao.addTrain("TrainName", 50, new String[]{"Moscow", "Berlin"},
                new Date[]{DateUtils.parse("HH:mm", "12:30"), DateUtils.parse("HH:mm", "16:00")});
    }

    @Test
    public void viewPassengersInfoTest() {
        List<Passenger> passengers = dao.viewPassengersInfo("ICE");
        Passenger first = passengers.get(0);
        Passenger second = passengers.get(1);
        TestCase.assertEquals("Kurt", first.getFirstName());
        TestCase.assertEquals("Daniel", second.getFirstName());
    }
}
